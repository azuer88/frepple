# FrePPLe Docker Compose

Docker compose files for running frepple.

## Usage

(1) Run:
```
git clone https://gitlab.com/azuer88/frepple 
cd frepple
docker-compose up
```
(2) open 'localhost' in browser
