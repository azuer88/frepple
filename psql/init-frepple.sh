#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    create user frepple with password 'frepple' createrole;
    create database frepple encoding 'utf-8' owner frepple;
    create database scenario1 encoding 'utf-8' owner frepple;
    create database scenario2 encoding 'utf-8' owner frepple;
    create database scenario3 encoding 'utf-8' owner frepple;
EOSQL